const router = require("express").Router();
const RegisteredUserModel = require("../models/RegisteredUser");

router.post("/", async (req, res) => {
  try {
    const user = new RegisteredUserModel({
      fullname: req.body.fullname,
      telNumber: req.body.telNumber,
      adress: req.body.adress,
      inn: req.body.inn,
    });
    await user.save();
    return res.status(200).send({
      message: `Регистрация прошла успешно!`,
    });
  } catch (e) {
    return res.status(400).send(e);
  }
});

router.get("/", async (req, res) => {
  try {
    const registered_users = await RegisteredUserModel.find();
    res.send(registered_users);
  } catch (e) {
    res.status(500).send(e);
  }
});

module.exports = router;
