const mongoose = require("mongoose");
const CryptoJS = require("crypto-js");

const SECRET_KEY = "secret key 123";

const RegisteredUserSchema = new mongoose.Schema({
  fullname: {
    type: String,
    required: [true, 'Поле "Логин" обязательно для заполнения'],
  },
  telNumber: {
    type: String,
    required: [true, 'Поле "Номер телефона" обязателен для заполнения'],
  },
  adress: {
    type: String,
    required: [true, 'Поле "Адрес" обязателен для заполнения'],
  },
  inn: {
    type: String,
    required: [true, 'Поле "ИНН" обязателен для заполнения'],
  },
});

RegisteredUserSchema.set("toJSON", {
  transform: (doc, ret) => {
    const bytes = CryptoJS.AES.decrypt(ret.inn, SECRET_KEY);
    ret.inn = bytes.toString(CryptoJS.enc.Utf8);
    return ret;
  },
});

RegisteredUserSchema.pre("save", async function (next) {
  if (!this.isModified("inn")) return next();
  try {
    this.inn = await CryptoJS.AES.encrypt(this.inn, SECRET_KEY).toString();
    next();
  } catch (e) {
    return next(e);
  }
});

const RegisteredUserModel = mongoose.model(
  "RegisteredUserModel",
  RegisteredUserSchema
);

module.exports = RegisteredUserModel;
