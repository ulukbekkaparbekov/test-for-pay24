const mongoose = require("mongoose");
const bcrypt = require("bcrypt");
const { nanoid } = require("nanoid");

const SALT_WORK_FACTOR = 2;

const UserSchema = new mongoose.Schema({
  username: {
    type: String,
    required: [true, 'Поле "Логин" обязательно для заполнения'],
  },
  password: {
    type: String,
    required: [true, 'Поле "Пароль" обязательно для заполнения'],
  },
  token: {
    type: String,
    required: true,
  },
});
UserSchema.set("toJSON", {
  transform: (doc, ret) => {
    delete ret.password;
    return ret;
  },
});

UserSchema.pre("save", async function (next) {
  if (!this.isModified("password")) return next();
  try {
    const salt = await bcrypt.genSalt(SALT_WORK_FACTOR);
    this.password = await bcrypt.hash(this.password, salt);
    next();
  } catch (e) {
    return next(e);
  }
});

UserSchema.methods.checkPassword = function (password) {
  return bcrypt.compare(password, this.password);
};

UserSchema.methods.generateToken = function () {
  this.token = nanoid();
};

const UserModel = mongoose.model("UserModel", UserSchema);

module.exports = UserModel;
