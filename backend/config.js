const dotenv = require("dotenv").config();

if (dotenv.error) throw dotenv.error;
const rootPath = __dirname;

module.exports = {
  rootPath,
  database: `mongodb://${process.env.DB_HOST}/${process.env.DB_NAME}`,
  databaseOpt: {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useCreateIndex: true,
    useFindAndModify: false,
  },
};
