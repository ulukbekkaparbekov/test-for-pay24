const mongoose = require("mongoose");
const { nanoid } = require("nanoid");
const config = require("./config");
const UserModel = require("./models/User");

mongoose.connect(config.database, config.databaseOpt);

const db = mongoose.connection;

db.once("open", async () => {
  try {
    await mongoose.connection.db.dropDatabase();
    await UserModel.create({
      password: "adminadmin",
      username: "admin",
      token: nanoid(),
    });
  } catch (e) {
    console.log(e);
  }
  await db.close();
});
