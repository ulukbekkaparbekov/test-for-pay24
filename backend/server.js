const express = require("express");
const cors = require("cors");
const mongoose = require("mongoose");
const dotenv = require("dotenv").config();
const http = require("http");
const config = require("./config");
const users = require("./app/users");
const registeredUsers = require("./app/registered-users");

const app = express();

if (dotenv.error) throw dotenv.error;

app.use(express.json());
app.use(cors());

mongoose
  .connect(config.database, config.databaseOpt)
  .catch((e) => console.log(config.database))
  .then(() => console.log("<<< MongoDB is worked! >>>"));

app.use("/users", users);
app.use("/registered-users", registeredUsers);

app.use((req, res) => {
  res.status(404).send({ error: "404 Not found" });
});

const server = http.createServer(app);

server.listen(3333, () => {
  console.log(`Server started at http://localhost:${3333}`);
});
