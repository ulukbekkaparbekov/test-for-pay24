export const saveToLocalStorage = (state) => {
  try {
    const serializedState = JSON.stringify(state);
    localStorage.setItem("statePay24", serializedState);
  } catch (e) {
    console.log("Не сохраниолсь");
  }
};

export const loadFromLocalStorage = () => {
  try {
    const serializedState = localStorage.getItem("statePay24");
    if (serializedState === null) {
      return undefined;
    }
    return JSON.parse(serializedState);
  } catch (e) {
    return undefined;
  }
};
