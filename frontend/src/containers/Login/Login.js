import { useState, useEffect, useContext } from "react";
import Button from "../../components/UI/Button/Button";
import TextField from "../../components/UI/Forms/TextField/TextField";
import axiosApi from "../../axiosApi";
import { useHistory } from "react-router-dom";
import { saveToLocalStorage } from "../../localStorage";
import { Context } from "../../Context";
import "./Login.css";

export default function Login() {
  const history = useHistory();
  const [isRequest, setIsRequest] = useState(false);
  const { setUser } = useContext(Context);

  const [form, setForm] = useState({
    username: "",
    password: "",
  });

  useEffect(() => {
    return () => {
      setForm({
        username: "",
        password: "",
      });
    };
  }, []);

  const inputChangeHandler = (e) => {
    const name = e.target.name;
    const value = e.target.value;

    setForm((prevState) => {
      return { ...prevState, [name]: value };
    });
  };

  const formSubmitHandler = async (e) => {
    e.preventDefault();
    setIsRequest(true);
    async function registUser() {
      let data = await axiosApi.post("/users/sessions", form).catch((err) => {
        err.response &&
          (err.response.status === 400
            ? alert("Заполните все поля!")
            : alert("Данные неверны!"));
      });
      await saveToLocalStorage(data.data);
      await setUser(data.data);
      history.push("/admin");
    }
    registUser().finally(() => {
      setIsRequest(false);
    });
  };
  return (
    <div className="login">
      <form onSubmit={formSubmitHandler}>
        <TextField
          type="text"
          required
          placeholder={"Имя пользователя"}
          name={"username"}
          id={"username"}
          labelFor={"username"}
          label={"Имя пользователя"}
          value={form.username}
          onChange={inputChangeHandler}
        />
        <TextField
          type="password"
          required
          placeholder={"Пароль"}
          name={"password"}
          id={"password"}
          labelFor={"password"}
          label={"Пароль"}
          value={form.password}
          onChange={inputChangeHandler}
        />
        <Button text="Отправить" type={"submit"} loading={isRequest} />
      </form>
    </div>
  );
}
