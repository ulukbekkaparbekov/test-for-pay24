import "./Admin.css";
import { useEffect, useState } from "react";
import axiosApi from "../../axiosApi";

export default function Admin() {
  const [state, setState] = useState([]);

  useEffect(() => {
    (async () => {
      const data = await axiosApi.get("/registered-users");
      setState(data.data);
    })();
  }, []);

  return (
    <div className="admin">
      <div className="container">
        <ul className="responsive-table">
          <li className="table-header">
            <div className="col col-2">ФИО</div>
            <div className="col col-3">Телефон</div>
            <div className="col col-4">ИНН</div>
            <div className="col col-4">Адрес</div>
          </li>
          {state.map((i) => {
            return (
              <li className="table-row" key={i._id}>
                <div className="col col-2" data-label="Customer Name">
                  {i.fullname}
                </div>
                <div className="col col-3" data-label="Amount">
                  {i.telNumber}
                </div>
                <div className="col col-4" data-label="Payment Status">
                  {i.inn}
                </div>
                <div className="col col-4" data-label="Payment Status">
                  {i.adress}
                </div>
              </li>
            );
          })}
        </ul>
      </div>
    </div>
  );
}
