import "./SuccessPage.css";

export default function SuccessPage() {
  return (
    <div>
      <h1 className={"main_title"}>Регистрация прошла успешна!</h1>
    </div>
  );
}
