import { useEffect, useState } from "react";
import { useHistory } from "react-router-dom";
import axiosApi from "../../axiosApi";
import Button from "../../components/UI/Button/Button";
import TextareaField from "../../components/UI/Forms/TextareaField/TextareaField";
import TextField from "../../components/UI/Forms/TextField/TextField";
import "./Home.css";

function Home() {
  let history = useHistory();
  const [form, setForm] = useState({
    fullname: "",
    telNumber: "",
    adress: "",
    inn: "",
  });

  useEffect(() => {
    return () => {
      setForm({
        fullname: "",
        telNumber: "",
        adress: "",
        inn: "",
      });
    };
  }, []);

  const [isRequest, setIsRequest] = useState(false);

  const inputChangeHandler = (e) => {
    const name = e.target.name;
    const value = e.target.value;

    setForm((prevState) => {
      return { ...prevState, [name]: value };
    });
  };

  const formSubmitHandler = async (e) => {
    e.preventDefault();
    setIsRequest(true);
    async function registUser() {
      await axiosApi
        .post("/registered-users", form)
        .then((res) => {
          history.push("/success");
        })
        .catch((err) => {
          err.response.status === 400 && alert("Заполните все поля!");
        });
    }
    registUser().finally(() => {
      setIsRequest(false);
    });
  };
  return (
    <div className="home">
      <form onSubmit={formSubmitHandler}>
        <TextField
          type="text"
          required
          placeholder={"ФИО"}
          name={"fullname"}
          id={"fullname"}
          labelFor={"fullname"}
          label={"ФИО"}
          value={form.fullname}
          onChange={inputChangeHandler}
        />
        <TextField
          type="number"
          required
          placeholder={"Номер телефона"}
          name={"telNumber"}
          id={"telNumber"}
          labelFor={"telNumber"}
          label={"Номер телефона"}
          value={form.telNumber}
          onChange={inputChangeHandler}
        />
        <TextareaField
          required
          placeholder={"Адрес"}
          name={"adress"}
          id={"adress"}
          labelFor={"adress"}
          label={"Адрес"}
          value={form.adress}
          onChange={inputChangeHandler}
        />
        <TextField
          type="number"
          required
          placeholder={"ИНН"}
          name={"inn"}
          id={"inn"}
          labelFor={"inn"}
          label={"ИНН"}
          value={form.inn}
          onChange={inputChangeHandler}
        />
        <Button loading={isRequest} text="Зарегистрироваться" type={"submit"} />
      </form>
    </div>
  );
}

export default Home;
