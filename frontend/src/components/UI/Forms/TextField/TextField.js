import "./TextField.css";

export default function TextField(props) {
  return (
    <div className="form__group field">
      <input
        type={props.type}
        className="form__field"
        placeholder={props.placeholder}
        name={props.name}
        id={props.id}
        value={props.value}
        required={props.required}
        onChange={props.onChange}
      />
      <label htmlFor={props.labelFor} className="form__label">
        {props.label}
      </label>
    </div>
  );
}
