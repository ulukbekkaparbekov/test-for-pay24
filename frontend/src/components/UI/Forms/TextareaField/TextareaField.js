import "./TextareaField.css";

export default function TextareaField(props) {
  return (
    <div className="form__group field">
      <textarea
        className="form__field"
        placeholder={props.placeholder}
        name={props.name}
        id={props.id}
        required={props.required}
        onChange={props.onChange}
      />
      <label htmlFor={props.labelFor} className="form__label">
        {props.label}
      </label>
    </div>
  );
}
