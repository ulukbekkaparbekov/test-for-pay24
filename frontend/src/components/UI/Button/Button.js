import "./Button.css";

export default function Button({ text, type, loading, children }) {
  return (
    <button disabled={loading} type={type} className="button">
      {loading ? (
        <svg width="20" viewBox="0 0 90 90" xmlns="http://www.w3.org/2000/svg">
          <circle
            id="c"
            fill="none"
            strokeWidth="4"
            strokeLinecap="round"
            stroke="blue"
            cx="45"
            cy="45"
            r="43"
          />
        </svg>
      ) : (
        text
      )}
    </button>
  );
}
