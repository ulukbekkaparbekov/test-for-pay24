import "./Layout.css";
import { useContext } from "react";
import { Context } from "../../Context";
import menuIcon from "../../assets/icons/menu.svg";
import { Link } from "react-router-dom";
import { saveToLocalStorage } from "../../localStorage";
import axsioApi from "../../axiosApi";

const Layout = ({ children }) => {
  const { user, setUser } = useContext(Context);
  const logout = async () => {
    await saveToLocalStorage("");
    await setUser(false);
    await axsioApi.delete("/users/sessions", {
      headers: {
        Authorization: user.token,
      },
    });
  };
  return (
    <div>
      <header className="header">
        <nav className="container navigation">
          <div className="menu">
            <img className="menuIcon" src={menuIcon} alt="menuIcon" />
          </div>
          <ul className="navigation__in">
            <li className="navigation__item">
              {user && (
                <Link className={"navigation__link"} onClick={logout} to={"/"}>
                  Выход
                </Link>
              )}
              <Link
                className={"navigation__link"}
                to={user ? "/admin" : "/login"}
              >
                Войти
              </Link>
            </li>
          </ul>
        </nav>
      </header>
      <main className={"container content"}>{children}</main>
    </div>
  );
};

export default Layout;
