import { useState } from "react";
import Layout from "./components/Layout/Layout";
import { Switch, Route, Redirect } from "react-router-dom";
import Home from "./containers/Home/Home";
import Admin from "./containers/Admin/Admin";
import { Context } from "./Context.js";
import Login from "./containers/Login/Login";
import NotFound from "./containers/NotFound/NotFound";
import SuccessPage from "./containers/SuccessPage/SuccessPage";
import { loadFromLocalStorage } from "./localStorage";

const ProtectedRoute = ({ isAllowed, redirectTo, ...props }) => {
  return isAllowed ? <Route {...props} /> : <Redirect to={redirectTo} />;
};

function App() {
  const [user, setUser] = useState(loadFromLocalStorage());
  return (
    <Context.Provider value={{ user, setUser }}>
      <Layout user={user}>
        <Switch>
          <Route path="/" exact>
            <Home />
          </Route>
          <ProtectedRoute
            isAllowed={!!user}
            path="/admin"
            exact
            component={Admin}
            redirectTo="/login"
          />
          <ProtectedRoute
            isAllowed={!user}
            path="/login"
            exact
            component={Login}
            redirectTo="/"
          />
          <Route path="/success" exact>
            <SuccessPage />
          </Route>
          <Route path="*">
            <NotFound />
          </Route>
        </Switch>
      </Layout>
    </Context.Provider>
  );
}

export default App;
